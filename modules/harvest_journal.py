#!/usr/bin/env python3

"""A module that provides functions for manipulating the 'Harvest Journal' add-on
in Nav."""

from pathlib import Path
import time

import pandas as pd
import pyautogui

from modules import ht_pyautogui


# # # Globals / Logger # # #

# # Path # #

Data = Path("data")
Logs = Data / "logs"
Png = Data / "png"
Csv = Data / "csv"

# Data Tables
Strains = Csv / "Harvest Journal Strains.csv"
Harvests_Report = Csv / "HarvestsReport.csv"

# PNG Files
Harvest_JournalPNG = str(Png / "harvestJournal.png")  # str/Path
Edit_Harvest_JournalPNG = str(Png / "editHarvestJournal.png")  # str/Path
Creation_DatePNG = str(Png / "creationDate.png")  # str/Path
Sort_DescendingPNG = str(Png / "sortDescending.png")  # str/Path
Creation_Date_SortedPNG = str(Png / "creationDateSorted.png")  # str/Path
Create_PackagePNG = str(Png / "createPackage.png")  # str/Path


def open_nav() -> None:
    """Uses PyAutoGUI to use keyboard shortcuts to open 'Microsoft NAV',
    using appropriate wait times."""
    time.sleep(1.0)
    # Start Menu won't open if super is pressed immediately
    pyautogui.run("k'win' w'Dynamics NAV' s0.7 k'enter'")
    # Wait 0.5 for Windows to catchup
    return None


def open() -> None:
    """Open 'Harvest Journal', a 'Microsoft NAV' addon, and snaps it to
    fullscreen.
    Assumes Nav is not already open."""
    open_nav()  # Wait for NAV to load and open Harvest Journal
    ht_pyautogui.wait_click(Harvest_JournalPNG)  # Open Harvest Journal
    ht_pyautogui.snap_fullscreen(ht_pyautogui.wait_for(Edit_Harvest_JournalPNG,
                                 Timeout=20,
                                 Timeout_Command=ht_pyautogui.unsnap_fullscreen))
    # Wait for Harvest Journal window title
    return None


def export() -> pd.DataFrame:
    """Exports current 'Harvest Journal' data into a pandas.DataFrame by
    copying the table to the clipboard, then reading the clipboard.
    Assumes 'Harvest Journal' is already open."""
    # Right-click to Organize by Date
    ht_pyautogui.wait_rclick(Creation_DatePNG)
    ht_pyautogui.wait_click(Sort_DescendingPNG)
    ht_pyautogui.wait_for(Creation_Date_SortedPNG)

    # Copying after highlighting all, loads the whole table after a
    # loading-bar pop-up
    pyautogui.hotkey("ctrl", 'a')  # Highlight whole table
    pyautogui.hotkey("ctrl", "shift", 'c')  # Copy highlighted cells
    time.sleep(20)  # Wait to load NAV to the clipboard

    # Get current batch names and waste values from clipboard
    return pd.read_clipboard(sep='\t', thousands=',')


def calculate_waste_to_enter() -> pd.DataFrame:
    """Uses the latest 'HarvestsReport.xls' from Metrc and the most
    recent entries in 'Harvest Journal' to calculate the waste that
    still needs to be entered into 'Harvest Journal'.

    Assumes 'Harvest Journal' is already open.

    Asserts index equivalency (for merge and intersection).

    Returns a pandas.DataFrame object with the harvest batches as the
    key/index and 'Waste' as the column label."""
    # Files and Diectories

    # Export and Load Data Tables
    entered = export()  # Load current 'Harvest Journal'
    metrc = pd.read_csv(Harvests_Report)  # Load Harvest Report

    # Reformat to match harvest batches between Metrc and 'Harvest
    # Journal'
    entered.rename(columns={"Harvest Batch No.": "Harvest Name"},
                   inplace=True)  # Match batch name with Metrc
    # Force all batches to uppercase
    # logger.debug("metrc['Harvest Name'].apply(str.upper)")  # Add logging
    metrc["Harvest Name"] = metrc["Harvest Name"].apply(str.upper)

    # Use "Harvest Name" to Pull Weights
    entered.set_index("Harvest Name", inplace=True)
    metrc.set_index("Harvest Name", inplace=True)

    to_enter = pd.merge(entered["Waste"], metrc["Waste"], how="inner",
                        on="Harvest Name", suffixes=("_Metrc", "_Entered"))
    to_enter["Waste"] = to_enter["Waste_Metrc"] - to_enter["Waste_Entered"]

    return to_enter


def read_batch() -> str:
    """Reads and extracts batch from the clipboard using Pandas."""
    return str(pd.read_clipboard(sep='\t')["Harvest Batch No."].values[0])


def read_active_batch() -> str:
    """Uses PyAutoGUI to copy the active/highlighted row to the
    clipboard, then calls read_batch as its return values to read and
    extract the batch from the entry data."""
    pyautogui.hotkey("ctrl", "shift", 'c')  # Copy active
    return read_batch()


def jump_down(Line_Count: int) -> None:
    """Presses the down arrow 'Line_Count' times in order to skip ahead when
    entering waste."""
    pyautogui.press("down", presses=Line_Count, interval=0.01)
    answer = pyautogui.confirm("Begin data entry here?", buttons=("Yes", "Quit"))
    if answer == "Quit":
        exit(0)  # User requested quit
    return None
