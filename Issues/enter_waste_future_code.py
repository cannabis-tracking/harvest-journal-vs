"""Figure Dominacy and Strains Used"""
Not_In_Nav = ["?", "Recovery", "Serenity", "Tee Box"]

# Account for waste already in Harvest Journal

strains_used = list(metrc.loc[batch]["Strains"] for batch in
		to_enter.index)  # Get strains found in df 'metrc'

# Deal with 'Harvest Journal' to Metrc strain issues
# Replace nested pd.Series values with the strain data they contain
for index, strain in enumerate(strains_used):
if isinstance(strain, pd.Series):  # type(strain) is pd.Series:
    # Assumes all strains are the same
    strains_used[index] = strain.iloc[0]


# ###################### CWD: ItemID Selection ###################### #
strains = pd.read_csv(HJ_Strains)  # Load from file instead
strains.set_index("Strain", inplace=True)  # Lookup dominancy by strain

# Remove strains not found in Nav
strains = strains[~strains.index.isin(Not_In_Nav)]
workaround_strainlist = []  # Temporary List
for strain in strains_used:
if strain not in Not_In_Nav:
    logger.debug(f"Adding {strain} to strains_used")
    workaround_strainlist.append(strain)
strains_used = workaround_strainlist

dominancy = pd.DataFrame([strains.loc[tgs.custom_capwords(strain)]
                         for strain in strains_used])
dominancy = [ItemId[dominant] for dominant in dominancy["Dominancy"]]

# Add dominancy to to_enter
to_enter["Dominancy"] = pd.DataFrame({"Dominancy": dominancy})
# ###################### CWD: ItemID Selection ###################### #

# Using whole dataframe, not just waste
to_enter = pd.merge(entered, metrc, how="inner",
                        on="Harvest Name", suffixes=("_Metrc", "_Entered"))

"""Future Feature: Better validation between databases"""
    # Log batches in Nav without data in to_enter
    if batch not in to_enter_index.values.tolist():
        logger.critical(f"Batch: {batch} not found in Metrc")
        msg = "Batch not found! Enter set_trace()?"
        if pyautogui.confirm(msg) == "OK":
            set_trace()
        else:
           continue


"""harvest_journal.calculate_waste_to_enter()"""
"""Future Testing
    # Whole DataFrame
    to_enter = pd.merge(entered, metrc, how="inner",
                        on="Harvest Name", suffixes=("_Metrc", "_Entered"))
    # Get indeces found in both dataframes
    to_enter_index = entered.index & metrc.index  # Intersection
    assert to_enter_index == to_enter.index  # Assert same result
    """
