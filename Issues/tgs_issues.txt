custom_words = {"8 BIT": "8-Bit",  # Reason: Hyphen
                    "BLUEBIRD OG": "Bluebird OG",  # Reason: Acronym capitalization
                    "CHEM OG": "Chem OG",  # Acronym capitalization
                    "ELMERS X JOURNEYMAN": "Elmers x Journey Man",
                    # Reason: Cross strain 'x' capitalization and 'Man'
                    "GENERAL TSO COOKIES": "General Tso's Cookies",
                    # Apostrophe and s not showing up (Metrc/NAV
                    # discrepancy?)
                    "GRAPE TANG X (LUPE SHINE X G FUNK)": "Grape Tang x (Lupe Shine x G Funk)",
                    # Parenthesis looks like first letter
                    "LOHIGH": "LoHigh"}
                    # Reason: Odd capitalization

if Text.upper() in custom_words: -> if Text in Strain_Map
	return custom_words[Text.upper()] -> return Strain_Map[Text]